/* eslint-disable no-undef */

import { validateDNASequence } from '../src/dna/validations/validate_dna_sequence'

describe('Función que verifica si la secuencia de ADN ingresada es válida', () => {
  test('Secuencia de ADN vacía', () => {
    const result = validateDNASequence([])

    expect(result.status).toBe(false)
  })
  test('Formato de secuencia de ADN incorrecta', () => {
    const DNASequence = [
      'AA',
      'AAA',
      'AAAA',
      'AAAAA'
    ]

    const result = validateDNASequence(DNASequence)

    expect(result.status).toBe(false)
  })
  test('Valor de la secuencia de ADN incorrecta', () => {
    const DNASequence = [
      'GCGG',
      'AOAO',
      'TCYY',
      'OAO0'
    ]

    const result = validateDNASequence(DNASequence)

    expect(result.status).toBe(false)
  })
})
