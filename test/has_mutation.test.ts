/* eslint-disable no-undef */
import { hasMutation } from '../src/dna/mutation/has_mutation'

describe('Probando la función hasMutation', () => {
  test('ADN con mutaciones, Debe de devolver true', () => {
    const DNASequence = ['ATGCGA', 'CAGTGC', 'TTATGT', 'AGAAGG', 'CCCCTA', 'TCACTG']

    const mutationExist = hasMutation(DNASequence)

    expect(mutationExist).toBe(true)
  })

  test('ADN sin mutaciones, Debe de devolver false', () => {
    const DNASequence = ['ATGCGA', 'CAGTGC', 'TTATTT', 'AGACGG', 'GCGTCA', 'TCACTG']

    const mutationExist = hasMutation(DNASequence)

    expect(mutationExist).toBe(false)
  })
})
