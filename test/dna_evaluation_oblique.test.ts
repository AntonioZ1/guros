import { DNAEvaluationOblique } from '../src/dna/mutation/evaluation_oblique/dna_evaluation_oblique'

/* eslint-disable no-undef */
describe('Probando algoritmo que detecta si una mutación existe de forma oblicua', () => {
  const analyze = (DNASequence: string[]): boolean => {
    const algorithm = new DNAEvaluationOblique()

    for (let indexA = 0; indexA < DNASequence.length; indexA++) {
      for (let indexB = 0; indexB < DNASequence[indexA].length; indexB++) {
        if (algorithm.hasMutation(DNASequence, indexA, indexB) === true) {
          return true
        }
      }
    }

    return false
  }

  test('ADN con mutaciones, oblicua A, debe de devolver true', () => {
    const DNASequence = [
      'TAAA',
      'ATAA',
      'AATA',
      'AAAT'
    ]

    expect(analyze(DNASequence)).toBe(true)
  })

  test('ADN con mutaciones, oblicua B, debe de devolver true', () => {
    const DNASequence = [
      'AAAG',
      'AAGA',
      'AGAA',
      'GAAA'
    ]

    expect(analyze(DNASequence)).toBe(true)
  })

  test('ADN sin mutaciones, debe de devolver false', () => {
    const DNASequence = [
      'GAAT',
      'AGTA',
      'AGTA',
      'GAAT'
    ]

    expect(analyze(DNASequence)).toBe(false)
  })
})
