import { DNAEvaluationVertical } from '../src/dna/mutation/evaluation_vertical/dna_evaluation_vertical'

/* eslint-disable no-undef */
describe('Probando algoritmo que detecta si una mutación existe de forma vertical', () => {
  const analyze = (DNASequence: string[]): boolean => {
    const algorithm = new DNAEvaluationVertical()

    for (let indexA = 0; indexA < DNASequence.length; indexA++) {
      for (let indexB = 0; indexB < DNASequence[indexA].length; indexB++) {
        if (algorithm.hasMutation(DNASequence, indexA, indexB) === true) {
          return true
        }
      }
    }

    return false
  }

  test('ADN con mutaciones, Debe de devolver true', () => {
    const DNASequence = [
      'GAAA',
      'AAAA',
      'TATA',
      'AAAA'
    ]

    expect(analyze(DNASequence)).toBe(true)
  })

  test('ADN sin mutaciones, Debe de devolver false', () => {
    const DNASequence = [
      'TAAA',
      'ATAA',
      'GATA',
      'AAAT'
    ]

    expect(analyze(DNASequence)).toBe(false)
  })
})
