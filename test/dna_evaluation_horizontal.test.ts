import { DNAEvaluationHorizontal } from '../src/dna/mutation/evaluation_horizontal/dna_evaluation_horizontal'

/* eslint-disable no-undef */
describe('Probando algoritmo que detecta si una mutación existe de forma horizontal', () => {
  const analyze = (DNASequence: string[]): boolean => {
    const algorithm = new DNAEvaluationHorizontal()

    for (let indexA = 0; indexA < DNASequence.length; indexA++) {
      for (let indexB = 0; indexB < DNASequence[indexA].length; indexB++) {
        if (algorithm.hasMutation(DNASequence, indexA, indexB) === true) {
          return true
        }
      }
    }

    return false
  }

  test('ADN con mutaciones, Debe de devolver true', () => {
    const DNASequence = [
      'TTTT',
      'TTAT',
      'TTAT',
      'TTAT'
    ]

    expect(analyze(DNASequence)).toBe(true)
  })

  test('ADN sin mutaciones, Debe de devolver false', () => {
    const DNASequence = [
      'TTAT',
      'TTAT',
      'TTAT',
      'TTAT'
    ]

    expect(analyze(DNASequence)).toBe(false)
  })
})
