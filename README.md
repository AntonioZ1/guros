# Guros
## _Desafío Sr. Full Stack Developer_

Aplicación que detecta si una persona tiene diferencias genéticas basándose en su secuencia de ADN.

## Instalación

Requieres tener instalado [Node.js](https://nodejs.org/) en tu equipo

Clona el repositorio, accede al directorio de la aplicación e instala las dependencias

```sh
cd guros
npm install
```

## Variables de entorno 
Dentro del repositorio encontrarás un archivo de ejemplo con el nombre **.env.example**, este contiene la definición de las variables de entorno empleadas en la aplicación.
Crea un archivo **.env** y pega dentro lo que se encuentre en el archivo **.env.example** 

## Comandos NPM disponibles

#### Ejecución de pruebas
Para iniciar la ejecución de las pruebas disponibles, ejecuta el siguiente comando en tu terminal
```sh
npm run test
```

#### Entorno de desarrollo
Para iniciar la aplicación en un entorno de **desarrollo**, ejecuta el siguiente comando en tu terminal
```sh
npm run dev
```
Este comando levantará un servidor en el puerto que definas en tu archivo **.env**.
Accede a la ruta raiz de la aplicación mediante tu navegador para consultar los end-points disponibles

#### Compilación
Para compilar los archivos **.ts**, ejecuta el siguiente comando en tu terminal
```sh
npm run build
```
Este comando compilará todos los archivos con extensión **.ts** que se encuentren dentro del directorio **/src**, depositando el JavaScript resultante en el directorio **/dist** 

#### Servidor ejecutado a partir del JavaScript resultante de la compilación
Para iniciar la ejecución del servidor con código JavaScript compilado, ejecuta el siguiente comando en tu terminal
```sh
npm start
```
> Nota: Asegurate de haber ejecutado primero el comando **npm run build** 

## Documentación
Accede al siguiente enlace para consultar la [Documentación](http://52.52.251.233)

## URL de la API
http://52.52.251.233

## Patrónes de diseño implementados
Para la evaluación de las secuencias de ADN se implementaron 2 patrónes de diseño
### Template Method
Se utilizó este patrón para reutilizar la detección de mutaciones en una secuencia de ADN, siendo sus clases concretas la definición de que base nitrogenada evaluar

### Strategy 
Se utilizó este patrón para seleccionar el algoritmo capas de evaluar secuencias ADN de forma oblicua

## Tecnologías implementadas

Esta aplicación fue construida a partir de los siguientes tecnologías

| Tecnologías | Enlace |
| ------- | ------ |
| TypeScript | https://www.typescriptlang.org/ |
| ts-node-dev | https://www.npmjs.com/package/ts-node-dev |
| Express JS | https://expressjs.com/es/ |
| dotenv | https://www.npmjs.com/package/dotenv |
| pg | https://www.npmjs.com/package/pg |
| TypeORM | https://typeorm.io/ |
| Eslint | https://eslint.org/ |
| Jest | https://jestjs.io/ |
| Swagger | https://swagger.io/ |
| NodeJS | https://nodejs.org/es/ |
| JavaScript | https://developer.mozilla.org/es/docs/Web/JavaScript |
| Git | https://git-scm.com/ |
| EC2 | https://aws.amazon.com/es/ec2/ |
| PostgreSQL | https://www.postgresql.org/ |