import dotenv from 'dotenv'
import { DataSource } from 'typeorm'
import { DNA } from './entity/dna.entity'
import { EvaluationDNA } from './entity/evaluation_dna.entity'
dotenv.config()

export const AppDataSource = new DataSource({
  type: 'postgres',
  host: 'localhost',
  port: Number(process.env.PG_PORT),
  username: process.env.PG_USERNAME,
  password: process.env.PG_PASSWORD,
  database: process.env.PG_DATABASE,
  entities: [EvaluationDNA, DNA],
  synchronize: true,
  logging: false
})
