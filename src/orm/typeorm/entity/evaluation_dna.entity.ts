import { Column, Entity, JoinTable, OneToMany, PrimaryGeneratedColumn } from 'typeorm'
import { DNA } from './dna.entity'

@Entity()
export class EvaluationDNA {
  @PrimaryGeneratedColumn()
  id?: number

  @Column()
  mutation?: boolean

  @Column()
  createdAt?: string

  @OneToMany(() => DNA, (dna) => dna.evaluation, { cascade: true })
  @JoinTable()
  dna!: DNA[]
}
