import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import { EvaluationDNA } from './evaluation_dna.entity'

@Entity()
export class DNA {
  constructor (dna: string) {
    this.dna = dna
  }

  @PrimaryGeneratedColumn()
  id!: number

  @Column()
  dna!: string

  @ManyToOne(() => EvaluationDNA, (evaluationDNA) => evaluationDNA.dna)
  evaluation!: EvaluationDNA
}
