import { DNAEvaluationHorizontal } from './evaluation_horizontal/dna_evaluation_horizontal'
import { DNAEvaluationOblique } from './evaluation_oblique/dna_evaluation_oblique'
import { DNAEvaluationVertical } from './evaluation_vertical/dna_evaluation_vertical'

/**
 * Detect if a person has a genetic
 * mutation from a DNA sequence
 * by applying 3 verification methods
 *
 * @param DNASequence
 * @returns boolean
 */
export const hasMutation = (DNASequence: string[]): boolean => {
  const obliqueDetection = new DNAEvaluationOblique()

  const verticalDetection = new DNAEvaluationVertical()

  const horizontalDetection = new DNAEvaluationHorizontal()

  for (let indexA = 0; indexA < DNASequence.length; indexA++) {
    for (let indexB = 0; indexB < DNASequence[indexA].length; indexB++) {
      if (obliqueDetection.hasMutation(DNASequence, indexA, indexB) === true) {
        return true
      }

      if (verticalDetection.hasMutation(DNASequence, indexA, indexB) === true) {
        return true
      }

      if (horizontalDetection.hasMutation(DNASequence, indexA, indexB) === true) {
        return true
      }
    }
  }

  return false
}
