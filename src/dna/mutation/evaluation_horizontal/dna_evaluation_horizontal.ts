import { DNAEvaluation } from '../dna_evaluation'
import { NitrogenousBase } from '../../interfaces/nitrogen_base.type'

/**
 * Class representing a Evaluation Horizontal
 * @extends DNAEvaluation
 * @see {@link https://refactoring.guru/es/design-patterns/template-method} Template Method - Design Pattern
 */
export class DNAEvaluationHorizontal extends DNAEvaluation {
  /**
   * Get the value of the nitrogenous base
   *
   * @override
   * @param DNASequence
   * @param indexA
   * @param indexB
   * @returns NitrogenousBase
   */
  protected getNitrogenousBase (DNASequence: string[], indexA: number, indexB: number): NitrogenousBase {
    return DNASequence[indexA][indexB] as NitrogenousBase
  }
}
