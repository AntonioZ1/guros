import { NitrogenousBase } from '../interfaces/nitrogen_base.type'

/**
 * Class representing a DNA Evaluation
 * @see {@link https://refactoring.guru/es/design-patterns/template-method} Template Method - Design Pattern
 */
export abstract class DNAEvaluation {
  private sequence: Map<NitrogenousBase, number>;

  /** Create a DNA Evaluation */
  constructor () {
    this.sequence = new Map()
  }

  /**
   * Detects if a person has a genetic
   * mutation from a DNA sequence
   *
   * @param DNASequence
   * @param indexA
   * @param indexB
   * @returns boolean
   */
  public hasMutation (DNASequence: string[], indexA: number, indexB: number): boolean {
    if (indexB === 0) {
      this.sequence.clear()
    }

    const nitrogenousBase = this.getNitrogenousBase(DNASequence, indexA, indexB)

    if (nitrogenousBase === null) {
      return false
    }

    this.analyze(nitrogenousBase)

    return this.sequence.get(nitrogenousBase) === 4
  }

  /**
   * Get the value of the nitrogenous base
   *
   * @param DNASequence
   * @param indexA
   * @param indexB
   * @returns NitrogenBase | null
   */
  protected abstract getNitrogenousBase(DNASequence: string[], indexA: number, indexB?: number): NitrogenousBase | null;

  /**
   * Look for possible mutations
   *
   * @param nitrogenousBase
   */
  private analyze (nitrogenousBase: NitrogenousBase): void {
    if (this.sequence.has(nitrogenousBase) === false) {
      this.sequence.clear()

      this.sequence.set(nitrogenousBase, 1)
    } else {
      this.sequence.set(nitrogenousBase, (this.sequence.get(nitrogenousBase) ?? 1) + 1)
    }
  }
}
