import { NitrogenousBase } from '../../interfaces/nitrogen_base.type'
import { ObliqueStrategy } from './oblique_strategy.interface'

/**
 * Class representing a Oblique A strategy
 * @implements ObliqueStrategy
 * @see {@link https://refactoring.guru/es/design-patterns/strategy} Strategy - Design Pattern
 */
export class ObliqueA implements ObliqueStrategy {
  private DNASequence: string[]

  private indexB: number

  /**
   * Create a Oblique A strategy
   *
   * @param DNASequence
   * @param indexB
   */
  constructor (DNASequence: string[], indexB: number) {
    this.DNASequence = DNASequence

    this.indexB = indexB
  }

  /**
   * Get value of the nitrogenous
   * base for this strategy
   *
   * @override
   * @returns NitrogenousBase
   */
  public getNitrogenousBase (): NitrogenousBase {
    return this.DNASequence[this.indexB][this.indexB] as NitrogenousBase
  }
}
