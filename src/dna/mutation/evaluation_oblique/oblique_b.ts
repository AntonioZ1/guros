import { ObliqueStrategy } from './oblique_strategy.interface'

/**
 * Class representing a Oblique B strategy
 * @implements ObliqueStrategy
 * @see {@link https://refactoring.guru/es/design-patterns/strategy} Strategy - Design Pattern
 */
export class ObliqueB implements ObliqueStrategy {
  private DNASequence: string[]

  private indexA: number

  private indexB: number

  /**
   * Create a Oblique B strategy
   *
   * @param DNASequence
   * @param indexA
   * @param indexB
   */
  constructor (DNASequence: string[], indexA: number, indexB: number) {
    this.DNASequence = DNASequence

    this.indexA = indexA

    this.indexB = indexB
  }

  /**
   * Get value of the nitrogenous
   * base for this strategy
   *
   * @override
   * @returns NitrogenBase
   */
  public getNitrogenousBase (): string {
    return this.DNASequence[(this.DNASequence[this.indexA].length - 1) - this.indexB][this.indexB]
  }
}
