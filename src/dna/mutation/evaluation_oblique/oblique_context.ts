import { NitrogenousBase } from '../../interfaces/nitrogen_base.type'
import { ObliqueStrategy } from './oblique_strategy.interface'

/**
 * Class representing a Oblique context
 * @see {@link https://refactoring.guru/es/design-patterns/strategy} Strategy - Design Pattern
 */
export class ObliqueContext {
  private obliqueStrategy!: ObliqueStrategy;

  /**
   * Set a oblique strategy
   *
   * @param strategy
   */
  setObliqueStrategy (strategy: ObliqueStrategy): void {
    this.obliqueStrategy = strategy
  }

  /**
   * get nitrogenous base to evaluate
   *
   * @returns NitrogenousBase
   */
  getNitrogenousBase (): NitrogenousBase {
    return this.obliqueStrategy.getNitrogenousBase() as NitrogenousBase
  }
}
