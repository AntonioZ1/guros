import { DNAEvaluation } from '../dna_evaluation'
import { NitrogenousBase } from '../../interfaces/nitrogen_base.type'
import { ObliqueA } from './oblique_a'
import { ObliqueB } from './oblique_b'
import { ObliqueContext } from './oblique_context'

/**
 * Class representing a Evaluation Oblique
 * @extends DNAEvaluation
 * @see {@link https://refactoring.guru/es/design-patterns/template-method} Template Method - Design Pattern
 */
export class DNAEvaluationOblique extends DNAEvaluation {
  /**
   * Get the value of the nitrogenous base
   *
   * @override
   * @param DNASequence
   * @param indexA
   * @param indexB
   * @returns NitrogenousBase | null
   */
  protected getNitrogenousBase (DNASequence: string[], indexA: number, indexB: number): NitrogenousBase | null {
    if (indexA >= 2) {
      return null
    }

    const context = new ObliqueContext()

    if (indexA === 0) {
      context.setObliqueStrategy(new ObliqueA(DNASequence, indexB))
    }

    if (indexA === 1) {
      context.setObliqueStrategy(new ObliqueB(DNASequence, indexA, indexB))
    }

    return context.getNitrogenousBase()
  }
}
