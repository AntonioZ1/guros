/**
 * Interface representing a Oblique strategy
 * @see {@link https://refactoring.guru/es/design-patterns/strategy} Strategy - Design Pattern
 */
export interface ObliqueStrategy {

  getNitrogenousBase(): string;

}
