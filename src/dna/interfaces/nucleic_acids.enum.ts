export enum NucleicAcids {
  ADENINE = 'A',
  THYMINE = 'T',
  CYTOSINE = 'C',
  GUANINE = 'G'
}
