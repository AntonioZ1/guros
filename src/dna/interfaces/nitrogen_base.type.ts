import { NucleicAcids } from './nucleic_acids.enum'

export type NitrogenousBase = NucleicAcids.ADENINE | NucleicAcids.THYMINE | NucleicAcids.CYTOSINE | NucleicAcids.GUANINE
