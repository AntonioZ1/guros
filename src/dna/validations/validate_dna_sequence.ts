import { NucleicAcids } from '../interfaces/nucleic_acids.enum'

/**
 * Validation of a DNA sequence
 *
 * @param DNASequence
 * @returns string[]
 */
export const validateDNASequence = (DNASequence: string[]) : { status: boolean, message: string } => {
  if (Array.isArray(DNASequence) === false) {
    return {
      status: false,
      message: 'El ADN a evaluar requiere de ser de un arreglo de caracteres'
    }
  }

  if (DNASequence.length === 0) {
    return {
      status: false,
      message: 'No se ha encontrado algún ADN a evaluar'
    }
  }

  const DNASequenceString = DNASequence.join('')

  if ((DNASequenceString.length / DNASequence.length) !== DNASequence.length) {
    return {
      status: false,
      message: 'Formato de ADN inválido, el ADN debe de estar compuesto por NxN'
    }
  }

  if (!DNASequenceString.match(/^[ACGT]+$/)) {
    return {
      status: false,
      message: `Se ha detectado un acidos nucleico inválido, solo se permite ingresar el primer caracter, Ejemplo: Adenina = ${NucleicAcids.ADENINE},  Citosina = ${NucleicAcids.CYTOSINE}, Guanina = ${NucleicAcids.GUANINE}, timina = ${NucleicAcids.THYMINE}`
    }
  }

  return {
    status: true,
    message: 'Validations completed'
  }
}
