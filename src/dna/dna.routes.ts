import express, { Router } from 'express'
import { detectMutationController, getDNAStatsController } from './dna.controller'

const DNARouter: Router = express.Router()

DNARouter.post('/mutation', detectMutationController)
DNARouter.get('/stats', getDNAStatsController)

export default DNARouter
