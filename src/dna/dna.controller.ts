import { Request, Response, NextFunction } from 'express'
import { DNARepository } from './dna.repository'
import { hasMutation } from './mutation/has_mutation'
import { validateDNASequence } from './validations/validate_dna_sequence'

/**
 * Controller for detect a mutation
 *
 * @param request
 * @param response
 * @param next
 */
export const detectMutationController = async (request: Request, response: Response, next: NextFunction) => {
  try {
    const validations = validateDNASequence(request.body.dna)

    if (validations.status === false) {
      response
        .status(400)
        .json({
          code: 400,
          message: validations.message,
          data: {}
        })

      return
    }

    const dna: string[] = request.body.dna

    const result = hasMutation(dna)

    await new DNARepository().save(dna, result)

    if (result === false) {
      response
        .status(403)
        .json({
          code: 403,
          message: 'No se ha detectado una mutación en la secuencia de ADN enviada',
          data: {}
        })

      return
    }

    response
      .status(200)
      .json({
        code: 200,
        message: 'Mutación detectada en la secuencia de ADN enviada',
        data: {}
      })
  } catch (error) {
    next(error)
  }
}

/**
 *
 * get dna stats
 *
 * @param request
 * @param response
 * @param next
 */
export const getDNAStatsController = async (request: Request, response: Response, next: NextFunction) => {
  try {
    const dnaRepository = new DNARepository()

    const stats = await dnaRepository.getStats()

    response
      .status(200)
      .json({
        code: 200,
        message: 'Estadísticas obtenidas',
        data: stats
      })
  } catch (error) {
    next(error)
  }
}
