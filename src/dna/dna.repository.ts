import { AppDataSource } from '../orm/typeorm/data-source'
import { DNA } from '../orm/typeorm/entity/dna.entity'
import { EvaluationDNA } from '../orm/typeorm/entity/evaluation_dna.entity'
import { DNAStats } from './interfaces/dna_stats.interface'

export class DNARepository {
  /**
   * Save a DNA evaluated
   *
   * @param DNASequence
   * @param hasMutation
   * @returns Promise<EvaluationDNA>
   */
  public async save (DNASequence: string[], hasMutation: boolean): Promise<EvaluationDNA> {
    const evaluationDNA = new EvaluationDNA()

    evaluationDNA.mutation = hasMutation

    evaluationDNA.createdAt = new Date().toISOString()

    evaluationDNA.dna = DNASequence.map((dna: string) => new DNA(dna))

    return await AppDataSource.getRepository(EvaluationDNA).save(evaluationDNA)
  }

  public async getStats (): Promise<DNAStats> {
    const evaluations: EvaluationDNA[] = await AppDataSource.getRepository(EvaluationDNA).find()

    const mutations: number = evaluations.filter((evaluation: EvaluationDNA) => evaluation.mutation === true).length

    const noMutations: number = evaluations.length - mutations

    return {
      count_mutations: mutations,
      count_no_mutation: noMutations,
      ratio: (noMutations === 0) ? 0 : mutations / noMutations
    }
  }
}
