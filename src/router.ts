import express, { Router } from 'express'
import DNARouter from './dna/dna.routes'

const router: Router = express.Router()

router.use(DNARouter)

export default router
