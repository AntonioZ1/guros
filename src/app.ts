import 'reflect-metadata'
import express, { json, urlencoded } from 'express'
import router from './router'
import { AppDataSource } from './orm/typeorm/data-source'
import cors from 'cors'
import swaggerUi from 'swagger-ui-express'
import { swagger } from './swagger'

const app = express()

app.use(cors())
app.use(urlencoded({ extended: true }))
app.use(json())
app.use(router)
app.use('/', swaggerUi.serve, swaggerUi.setup(swagger))

app.listen(process.env.PORT, async () => {
  await AppDataSource.initialize()
  console.log(`Server listen to http://localhost:${process.env.PORT}`)
})
