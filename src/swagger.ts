export const swagger = {
  swagger: '2.0',
  info: {
    description: 'Detecta si una persona tiene diferencias genéticas basándose en su secuencia de ADN.',
    version: '1.0.0',
    title: 'Desafío Sr. Full Stack Developer'
  },
  host: '52.52.251.233',
  schemes: [
    'http'
  ],
  paths: {
    '/mutation': {
      post: {
        tags: [
          'ADN'
        ],
        summary: 'Detecta si una persona tiene diferencias genéticas',
        consumes: [
          'application/json'
        ],
        produces: [
          'application/json'
        ],
        parameters: [
          {
            in: 'body',
            name: 'dna',
            description: 'Secuencia de ADN a evaluar',
            required: true,
            schema: {
              $ref: '#/definitions/DNASequence'
            }
          }
        ],
        responses: {
          200: {
            description: 'Se ha encontrado una mutación en la secuencia de ADN'
          },
          400: {
            description: 'La secuencia de ADN enviada es incorrecta'
          },
          403: {
            description: 'No se ha detectado una mutación en la secuencia de ADN enviada'
          },
          500: {
            description: 'Error inesperado'
          }
        }
      }
    },
    '/stats': {
      get: {
        tags: [
          'ADN'
        ],
        summary: 'Obtiene las estadísticas de las verificaciones de ADN realizadas',
        consumes: [
          'application/json'
        ],
        produces: [
          'application/json'
        ],
        responses: {
          200: {
            description: 'Estadísticas obtenidas'
          },
          500: {
            description: 'Error inesperado'
          }
        }
      }
    }
  },
  definitions: {
    DNASequence: {
      type: 'array',
      example: {
        dna: ['ATGCGA', 'CAGTGC', 'TTATTT', 'AGACGG', 'GCGTCA', 'TCACTG']
      },
      items: {
        type: 'string'
      }
    }
  }
}
